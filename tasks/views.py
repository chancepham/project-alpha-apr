# tasks/views.py
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from .models import Task
from django.utils import timezone

@login_required
def delete_task(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if request.method == 'POST':
        task.delete()
        return redirect('show_my_tasks')
    return render(request, 'tasks/confirm_delete.html', {'task': task})
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    return render(request, "tasks/create_task.html", {"form": form})


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)

    now = timezone.now()
    print(f"Current time: {now}")

    for task in tasks:
        print(f"Task due date: {task.due_date}")
        if task.due_date > now:
            task.countdown = (task.due_date - now).total_seconds()
            print(f"Countdown: {task.countdown} seconds")
        else:
            task.countdown = -1
            print("Task is overdue")

    return render(request, "tasks/show_my_tasks.html", {"tasks": tasks})
