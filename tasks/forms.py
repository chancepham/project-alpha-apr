# forms.py
from django import forms
from .models import Task

class TaskForm(forms.ModelForm):
    start_date = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'class': 'form-control',
                'placeholder': 'mm/dd/yyyy hh:mm'
            },
            format='%Y-%m-%dT%H:%M'
        ),
        input_formats=['%Y-%m-%dT%H:%M'],
        error_messages={
            'invalid': 'Enter a valid date and time in yyyy-mm-dd hh:mm format.',
        }
    )
    due_date = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'class': 'form-control',
                'placeholder': 'mm/dd/yyyy hh:mm'
            },
            format='%Y-%m-%dT%H:%M'
        ),
        input_formats=['%Y-%m-%dT%H:%M'],
        error_messages={
            'invalid': 'Enter a valid date and time in yyyy-mm-dd hh:mm format.',
        }
    )

    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]

    def __init__(self, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        self.fields['due_date'].input_formats = ['%Y-%m-%dT%H:%M']
        self.fields['start_date'].input_formats = ['%Y-%m-%dT%H:%M']
