from django.urls import path
from .views import list_projects, show_project, create_project, delete_project

urlpatterns = [
    path("projects/", list_projects, name="list_projects"),
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path('delete/<int:project_id>/', delete_project, name='delete_project'),
]
